﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Distance.Query;
using Application.User.Command;
using Application.User.Query;
using Domain.Entity;
using Domain.View.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace User.Microservice.Controllers
{
    public class UsersController : BaseController
    {
        /// <summary>
        /// login and get token
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Login), Name = nameof(Login))]
        public async Task<UserToken> Login(Login login, CancellationToken cancellationToken)
        {
            return await Mediator.Send(login, cancellationToken);
        }
        /// <summary>
        /// register and get token
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(Register), Name = nameof(Register))]
        public async Task<UserToken> Register(Register register, CancellationToken cancellationToken)
        {
            return await Mediator.Send(register, cancellationToken);
        }
        [Authorize]
        [HttpGet(nameof(UserDistanceList), Name = nameof(UserDistanceList))]
        public async Task<List<PointDistance>> UserDistanceList()
        {
            return await Mediator.Send(new GetUserDistances());
        }
    }
}
