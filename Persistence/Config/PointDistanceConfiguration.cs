﻿using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Config
{
    public class PointDistanceConfiguration : IEntityTypeConfiguration<PointDistance>
    {
        public void Configure(EntityTypeBuilder<PointDistance> builder)
        {
            builder.HasOne<AppUser>().WithMany(m => m.PointDistances)
                .HasForeignKey("UserId").OnDelete(DeleteBehavior.Cascade);
        }
    }
}
