﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Point.Microservice.Controllers
{
    [Authorize]
    public class PointDistanceController : BaseController
    {
        private readonly IHttpContextAccessor _accessor;

        public PointDistanceController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        [HttpPost(nameof(CreateDistance), Name = nameof(CreateDistance))]
        public async Task CreateDistance(CreateDistanceDto createDistance, CancellationToken cancellationToken)
        {
            createDistance.UserId = _accessor.HttpContext.User.UserId();
            Uri uri = new Uri("rabbitmq://localhost/createDistanceQueue");
            var endPoint = await Bus.GetSendEndpoint(uri);
            await endPoint.Send(createDistance, cancellationToken);
        }
    }
}
