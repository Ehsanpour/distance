﻿using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Point.Microservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private IBus _bus;
        protected IBus Bus => _bus ??= HttpContext.RequestServices.GetService<IBus>();
    }
}
