﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Domain.Entity;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Distance.Query
{
   public class GetUserDistances : IRequest<List<PointDistance>>
   {
        public class GetUserDistancesHandler : IRequestHandler<GetUserDistances, List<PointDistance>>
        {
            private readonly Context _context;
            private readonly IHttpContextAccessor _accessor;
            public GetUserDistancesHandler(Context context, IHttpContextAccessor accessor)
            {
                _context = context;
                _accessor = accessor;
            }

            public async Task<List<PointDistance>> Handle(GetUserDistances request, CancellationToken cancellationToken)
            {
                var userId = _accessor.HttpContext.User.UserId();
                return await _context.PointDistances.Where(m => m.UserId == userId).ToListAsync(cancellationToken);
            }
        }
    }
}
