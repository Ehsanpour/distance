﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Domain;
using Domain.Dto;
using Domain.Entity;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Persistence;

namespace Application.Distance.Command
{
    public class CreateDistance : CreateDistanceDto, IRequest
    {
        public class CreateDistanceHandler : IRequestHandler<CreateDistance>
        {
            private readonly Context _context;
            private readonly UserManager<AppUser> _userManager;
            private readonly IHttpContextAccessor _accessor;
            public CreateDistanceHandler(Context context, UserManager<AppUser> userManager, IHttpContextAccessor accessor)
            {
                _context = context;
                _userManager = userManager;
                _accessor = accessor;
            }

            public async Task<Unit> Handle(CreateDistance request, CancellationToken cancellationToken)
            {
                var userId = _accessor.HttpContext.User.UserId();
                var user = await _userManager.FindByIdAsync(userId.ToString());
                if (user == null)
                    throw new RestException(HttpStatusCode.NotFound);
                var point = new PointDistance
                {
                    CreatedDate = DateTime.Now,
                    FirstPoint = request.FirstPoint,
                    SecondPoint = request.SecondPoint,
                    UserId = user.Id
                };
                _context.PointDistances.Add(point);
                var unit = await _context.SaveChangesAsync(cancellationToken) > 0;
                if (unit)
                    return Unit.Value;

                throw new RestException(HttpStatusCode.BadRequest);
            }
        }
    }
}
