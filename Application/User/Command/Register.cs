﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain.Dto;
using Domain.Entity;
using Domain.View.User;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Application.User.Command
{
    public class Register : RegisterDto, IRequest<UserToken>
    {
        public class RegisterHandler : IRequestHandler<Register, UserToken>
        {
            private readonly UserManager<AppUser> _userManager;
            private readonly IJwtGenerator _jwtGenerator;
            public RegisterHandler(UserManager<AppUser> userManager, IJwtGenerator jwtGenerator)
            {
                _userManager = userManager;
                _jwtGenerator = jwtGenerator;
            }

            public async Task<UserToken> Handle(Register request, CancellationToken cancellationToken)
            {
                var user = new AppUser
                {
                    UserName = request.Email,
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    EmailConfirmed = true
                };
                var result = await _userManager.CreateAsync(user, request.Password);
                if (result.Succeeded)
                {
                    var userView = new UserToken
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Token = await _jwtGenerator.CreateToken(user)
                    };
                    return userView;
                }

                throw new RestException(HttpStatusCode.BadRequest, result.Errors);
            }
        }
    }
}
