﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Application.Errors;
using Application.Interfaces;
using Domain.Dto;
using Domain.Entity;
using Domain.View.User;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace Application.User.Query
{
    public class Login : LoginDto, IRequest<UserToken>
    {
        public class LoginHandler : IRequestHandler<Login, UserToken>
        {
            private readonly UserManager<AppUser> _userManager;
            private readonly IJwtGenerator _jwtGenerator;
            public LoginHandler(UserManager<AppUser> userManager, IJwtGenerator jwtGenerator)
            {
                _userManager = userManager;
                _jwtGenerator = jwtGenerator;
            }

            public async Task<UserToken> Handle(Login request, CancellationToken cancellationToken)
            {
                var user = await _userManager.FindByNameAsync(request.EmailAddress);

                if (user == null)
                {
                    throw new RestException(HttpStatusCode.Unauthorized, new { formError = "Bad User Or Password" });
                }

                if (!user.EmailConfirmed)
                {
                    throw new RestException(HttpStatusCode.Locked
                        , new { account = "User is lock" });
                }
                var result = await _userManager.CheckPasswordAsync(user, request.Password);
                if (!result)
                    throw new RestException(HttpStatusCode.Unauthorized, new {formError = "Bad User Or Password"});

                var userView = new UserToken
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Token = await _jwtGenerator.CreateToken(user)
                };
                return userView;
            }
        }
    }
}
