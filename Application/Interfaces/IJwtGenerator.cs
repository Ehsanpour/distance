﻿using System.Threading.Tasks;
using Domain.Entity;

namespace Application.Interfaces
{
    public interface IJwtGenerator
    {
        Task<string> CreateToken(AppUser appUser);
    }
}
