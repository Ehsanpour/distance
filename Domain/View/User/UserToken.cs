﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.View.User
{
    public class UserToken
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Token { get; set; }
    }
}
