﻿using System;
using System.Security.Claims;


namespace Domain
{
    public static class MyTools
    {
        public static Guid UserId(this ClaimsPrincipal user)
        {
            return Guid.Parse(user.FindFirst(ClaimTypes.NameIdentifier).Value);
        }
    }
}
