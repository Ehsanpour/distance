﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Domain.Entity
{
    public class AppUserClaim : IdentityUserClaim<Guid>
    {
        public virtual AppUser AppUser { get; set; }
    }
}
