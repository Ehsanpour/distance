﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Domain.Entity
{
    public class AppRole : IdentityRole<Guid>
    {
        public bool IsSystemRole { get; set; }
        public ICollection<AppUserRole> UserRoles { get; set; }
    }
}
