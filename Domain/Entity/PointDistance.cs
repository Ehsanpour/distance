﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Domain.Entity
{
    public class PointDistance : BaseEntity
    {
        public float FirstPoint { get; set; }
        public float SecondPoint { get; set; }
        public Guid UserId { get; set; }
        public virtual AppUser User { get; set; }
    }
}
