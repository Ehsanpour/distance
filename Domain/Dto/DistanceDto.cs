﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entity;

namespace Domain.Dto
{
   public class CreateDistanceDto
    {
        public float FirstPoint { get; set; }
        public float SecondPoint { get; set; }
        public Guid? UserId { get; set; }
    }
}
