﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Domain.Dto
{
    public class LoginDto
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
    public class LoginDtoValidator : AbstractValidator<LoginDto>
    {
        public LoginDtoValidator()
        {
            RuleFor(m => m.EmailAddress).NotEmpty().EmailAddress();
            RuleFor(m => m.Password).NotEmpty();
        }
    }
}
