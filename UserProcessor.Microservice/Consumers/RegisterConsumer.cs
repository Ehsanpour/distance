﻿using System.Threading.Tasks;
using Domain.Dto;
using MassTransit;
using MediatR;

namespace UserProcessor.Microservice.Consumers
{
    public class RegisterConsumer : IConsumer<RegisterDto>
    {
        private readonly IMediator _mediator;

        public RegisterConsumer(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Consume(ConsumeContext<RegisterDto> context)
        {
            var data = context.Message;
            await _mediator.Send(data);
        }
    }
}
