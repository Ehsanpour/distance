﻿using Domain.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Entity;
using MassTransit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace UserProcessor.Microservice.Consumers
{
    public class CreateDistanceConsumer : IConsumer<CreateDistanceDto>
    {
        private readonly Context _context;
        private readonly UserManager<AppUser> _userManager;
        public CreateDistanceConsumer(Context context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task Consume(ConsumeContext<CreateDistanceDto> context)
        {
            var data = context.Message;
            var users = await _context.Users.ToListAsync();
            var user = await _userManager.FindByIdAsync(data.UserId.ToString());
            if (user != null)
            {
                var newPoint = new PointDistance
                {
                    CreatedDate = DateTime.Now,
                    FirstPoint = data.FirstPoint,
                    UserId = user.Id,
                    SecondPoint = data.SecondPoint
                };
                _context.PointDistances.Add(newPoint);
                await _context.SaveChangesAsync();
            }

        }
    }
}
