﻿using System.Threading.Tasks;
using Domain.Dto;
using MassTransit;
using MediatR;

namespace UserProcessor.Microservice.Consumers
{
    public class LoginConsumer : IConsumer<LoginDto>
    {
        private readonly IMediator _mediator;

        public LoginConsumer(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task Consume(ConsumeContext<LoginDto> context)
        {
            var data = context.Message;
            await _mediator.Send(data);
        }
    }
}
